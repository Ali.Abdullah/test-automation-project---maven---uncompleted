package src.Sources;
import java.io.File;
import org.apache.commons.io.FileUtils;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.PageObjectModel.Util.util.ScreenshotUtils;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;


//import io.github.bonigarcia.wdm.WebDriverManager;

public class AliPOMTest extends TestBase {

	public AliPOMTest() throws IOException {
		super();	
	}

	@Parameters("browser")
	@BeforeMethod
	public void PreConditions(Method method,String browser) throws ATUTestRecorderException {
//		WebDriverManager.chromedriver().setup();
//		System.setProperty("Webdriver.Chrome.driver", "Resources/chromedriver.exe");
		Base(browser);
		Video(method.getName());
	}
	
	@AfterMethod
	public void PostConditions() throws ATUTestRecorderException {
		driver.quit();
		 VideoEnd();
	}
	
	@Test(priority=1)
	public void Crm(Method method) throws IOException {
		Actions action = new Actions(driver);
		WebElement contact = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/a[3]/span[1]"));
		action.moveToElement(contact).contextClick().build().perform();
		WebElement settings = driver.findElement(By.xpath("(//i[@aria-hidden='true'])[3]"));
		settings.click();
		WebElement Navdown = driver.findElement(By.xpath("//span[text()='Settings']"));
		Navdown.click();
		WebElement Config = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/a[2]"));
		Config.click();
		WebElement Company = driver.findElement(By.xpath("(//*[@class='header'])[2]"));
		Company.click();
		WebElement CompanyName = driver.findElement(By.name("name"));
		CompanyName.sendKeys("EgySite Company LLC");
		WebElement Save1 = driver.findElement(By.xpath("//button[@class='ui linkedin small button']"));
		Save1.click();
		driver.navigate().back();
		WebElement contactFields = driver.findElement(By.xpath("(//*[@class='header'])[1]"));
		contactFields.click();
		WebElement FirstName = driver.findElement(By.name("first_name"));
		FirstName.sendKeys("ALI");
		WebElement MiddleName = driver.findElement(By.name("middle_name"));
		MiddleName.sendKeys("ABDULLAH");
		WebElement LastName = driver.findElement(By.name("last_name"));
		LastName.sendKeys("ABDELGHANY");
		WebElement Save2 = driver.findElement(By.xpath("//button[@class='ui linkedin small button']"));
		Save2.click();
		ScreenshotUtils.screenshot(method.getName());
	}
	
	
	
	
	

}
